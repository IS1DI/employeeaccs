package ru.aisa.employeeaccs.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String lastName;
    private String firstName;
    private String middleName;
    private LocalDate birthDate;
    @ManyToOne
    @JoinColumn(name = "department")
    private Department department;
    @Transient
    public String fullName() {
        return lastName + " " + firstName + " " + middleName;
    }
}