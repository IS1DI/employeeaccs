package ru.aisa.employeeaccs.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String name;
    private String description;
    private LocalDate createdAt;
    @OneToMany(mappedBy = "department", fetch = FetchType.EAGER)
    private Set<Employee> employees;
}