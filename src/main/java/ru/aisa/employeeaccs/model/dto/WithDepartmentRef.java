package ru.aisa.employeeaccs.model.dto;

public interface WithDepartmentRef {
    Long getDepartmentId();
}
