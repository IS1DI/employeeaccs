package ru.aisa.employeeaccs.model.dto;

import lombok.Data;

import java.time.LocalDate;

public class EmployeeDto {
    @Data
    public static final class Create {
        private String firstName;
        private String middleName;
        private String lastName;
        private LocalDate birthDate;
        private Long departmentId;

    }

    @Data
    public static final class Update implements WithDepartmentRef {
        private String firstName;
        private String middleName;
        private String lastName;
        private LocalDate birthDate;
        private Long departmentId;
    }

    @Data
    public static final class Output implements WithDepartmentRef {
        private Long id;
        private String fullName;
        private LocalDate birthDate;
        private Long departmentId;
        private String departmentName;
    }
}