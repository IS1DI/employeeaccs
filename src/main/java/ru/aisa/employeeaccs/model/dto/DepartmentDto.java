package ru.aisa.employeeaccs.model.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

public class DepartmentDto {
    @Data
    public static final class Create {
        private String name;
        private String description;
        private LocalDate createdAt;
    }

    @Data
    public static final class Update {

    }

    @Data
    public static final class Output {
        private Long id;
        private String name;
        private String description;
        private LocalDate createdAt;
        private Set<Long> employees;
    }
}