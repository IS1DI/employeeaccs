package ru.aisa.employeeaccs.model.mapper;

import org.mapstruct.*;
import ru.aisa.employeeaccs.model.dto.EmployeeDto;
import ru.aisa.employeeaccs.model.entity.Employee;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {
    @Mapping(target = "fullName", expression = "java(entity.fullName())")
    @Mapping(target = "departmentName", source = "department.name")
    @Mapping(target = "departmentId", source = "department.id")
    EmployeeDto.Output toOutput(Employee entity);

    @Mapping(target = "department.id", source = "departmentId")
    Employee toEntity(EmployeeDto.Create dto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Employee toUpdate(EmployeeDto.Update dto, @MappingTarget Employee entity);

    @Mapping(target = "departmentId", source = "department.id")
    EmployeeDto.Update map(Employee entity);
}