package ru.aisa.employeeaccs.model.mapper;

import org.mapstruct.Mapper;
import ru.aisa.employeeaccs.model.dto.DepartmentDto;
import ru.aisa.employeeaccs.model.entity.Department;
import ru.aisa.employeeaccs.model.entity.Employee;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface DepartmentMapper {
    DepartmentDto.Output toOutput(Department entity);

    Department toEntity(DepartmentDto.Create dto);

    default Set<Long> map(Set<Employee> employee) {
        if (employee == null) {
            return new HashSet<>();
        } else {
            return employee.stream().map(Employee::getId).collect(Collectors.toSet());
        }
    }
}