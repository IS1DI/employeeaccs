package ru.aisa.employeeaccs.events;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import lombok.Getter;

@Getter
public class CreateEntityEvent<E, C extends Component> extends ComponentEvent<C> {
    private final E entity;

    /**
     * Создает новое событие создания сущности используя переданные source компонент
     * с переданными: boolean значением, которое определяет стороны его отправки (клиент или сервер),
     * id созданной сущности
     *
     * @param entity     созданная сущности
     * @param source     компонент из которого передается событие
     * @param fromClient <code>true</code> Если событие идет со стороны клиента,
     *                   <code>false</code> иначе
     */
    public CreateEntityEvent(E entity, C source, boolean fromClient) {
        super(source, fromClient);
        this.entity = entity;
    }
}
