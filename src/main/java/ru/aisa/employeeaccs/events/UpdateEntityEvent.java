package ru.aisa.employeeaccs.events;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import lombok.Getter;

@Getter
public class UpdateEntityEvent<ID, E, C extends Component> extends ComponentEvent<C> {
    private final E entity;
    private final ID entityId;

    /**
     * Создает новое событие создания сущности используя переданные source компонент
     * с переданными: boolean значением, которое определяет стороны его отправки (клиент или сервер),
     * обновленной сущности
     *
     * @param entity     созданная сущность
     * @param source     компонент из которого передается событие
     * @param fromClient <code>true</code> Если событие идет со стороны клиента,
     *                   <code>false</code> иначе
     */
    public UpdateEntityEvent(ID entityId, E entity, C source, boolean fromClient) {
        super(source, fromClient);
        this.entity = entity;
        this.entityId = entityId;
    }
}
