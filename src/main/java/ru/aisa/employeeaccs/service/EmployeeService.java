package ru.aisa.employeeaccs.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aisa.employeeaccs.exception.NotFoundException;
import ru.aisa.employeeaccs.exception.NullPointerException;
import ru.aisa.employeeaccs.message.MessageMethod;
import ru.aisa.employeeaccs.model.dto.WithDepartmentRef;
import ru.aisa.employeeaccs.model.entity.Department;
import ru.aisa.employeeaccs.model.entity.Employee;
import ru.aisa.employeeaccs.repository.EmployeeRepository;

import java.util.Optional;
import java.util.function.BiFunction;

@Service
@Transactional
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final DepartmentService departmentService;

    public Employee create(Employee employee) {
        employee.setDepartment(
                Optional.ofNullable(employee.getDepartment()).map(Department::getId)
                        .map(departmentService::get)
                        .orElseThrow(() -> new NullPointerException(MessageMethod.DEPARTMENT_REQUIRED))
        );
        return employeeRepository.save(employee);
    }

    public Employee get(Long id) {
        return Optional.ofNullable(id).flatMap(employeeRepository::findById)
                .orElseThrow(() -> new NotFoundException(MessageMethod.EMPLOYEE_NOT_FOUND));
    }

    public <DTO> Employee update(Long id, DTO employeeNew, BiFunction<DTO, Employee, Employee> mapper) {
        Employee employee = mapper.apply(employeeNew, get(id));
        if (employeeNew instanceof WithDepartmentRef emp) {
            employee.setDepartment(departmentService.get(emp.getDepartmentId()));
        }
        return employee;
    }

    public Page<Employee> page(PageRequest pageRequest, Sort sort) {
        return employeeRepository.findAll(pageRequest.withSort(sort));
    }

    public long count() {
        return employeeRepository.count();
    }
}