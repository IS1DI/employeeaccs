package ru.aisa.employeeaccs.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aisa.employeeaccs.exception.NotFoundException;
import ru.aisa.employeeaccs.message.MessageMethod;
import ru.aisa.employeeaccs.model.entity.Department;
import ru.aisa.employeeaccs.repository.DepartmentRepository;

import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class DepartmentService {
    private final DepartmentRepository departmentRepository;

    public Department create(Department department) {
        return departmentRepository.save(department);
    }

    public Page<Department> page(PageRequest pageRequest, Sort sort) {
        return departmentRepository.findAll(pageRequest.withSort(sort));
    }

    public boolean nameExists(String name) {
        return departmentRepository.existsByName(name);
    }

    public Department get(Long id) {
        return Optional.ofNullable(id).flatMap(departmentRepository::findById)
                .orElseThrow(() -> new NotFoundException(MessageMethod.DEPARTMENT_NOT_FOUND));
    }

    public long count() {
        return departmentRepository.count();
    }
}