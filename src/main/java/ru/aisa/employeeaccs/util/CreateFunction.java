package ru.aisa.employeeaccs.util;

import java.util.function.Function;

@FunctionalInterface
public interface CreateFunction<T, R> extends Function<T, R> {
}
