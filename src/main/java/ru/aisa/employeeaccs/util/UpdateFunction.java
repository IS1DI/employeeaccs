package ru.aisa.employeeaccs.util;

import java.util.function.Function;

@FunctionalInterface
public interface UpdateFunction<T, R> extends Function<T, R> {
}
