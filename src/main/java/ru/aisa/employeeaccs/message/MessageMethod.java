package ru.aisa.employeeaccs.message;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum MessageMethod {
    EMPLOYEE_NOT_FOUND("employee.error.notFound"),
    DEPARTMENT_NOT_FOUND("department.error.notFound"),
    DEPARTMENT_REQUIRED("department.error.required");
    private final String messageCode;
}
