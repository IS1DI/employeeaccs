package ru.aisa.employeeaccs.routes;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.*;
import com.vaadin.flow.theme.lumo.Lumo;
import ru.aisa.employeeaccs.routes.department.DepartmentView;
import ru.aisa.employeeaccs.routes.employee.EmployeeView;

import java.util.HashMap;
import java.util.Map;

@Route("")
public class MainLayout extends AppLayout implements BeforeEnterObserver {
    public MainLayout() {
        var title = new H1(getTranslation("app.title"));
        title.getStyle()
                .set("font-size", "var(--lumo-font-size-l)")
                .set("margin", "0 auto 0 0")
                .set("padding-left", "var(--lumo-space-s)");
        RouteTabs tabs = tabs();
        addToNavbar(title);
        addToNavbar(true, tabs);
        addToNavbar(dropDown());
    }

    private MenuBar dropDown() {
        var menuBar = new MenuBar();
        var themeToggle = new Checkbox(getTranslation("app.header-box.dark-theme-toggle"));
        themeToggle.addValueChangeListener(e -> {
            setTheme(e.getValue());
        });
        getUI().map(UI::getSession).map(session -> session.getAttribute("dark-theme"))
                .ifPresent(v -> themeToggle.setValue((boolean) v));
        var localeChanger = new LocaleChanger();
        var settings = menuBar.addItem(getTranslation("app.header-box.title"));
        var subMenu = settings.getSubMenu();
        subMenu.addItem(themeToggle);
        subMenu.addItem(localeChanger);
        settings.addThemeNames();
        menuBar.addThemeVariants(MenuBarVariant.LUMO_END_ALIGNED);
        menuBar.getStyle().set("margin", "0 0 0 auto").set("padding-right", "var(--lumo-space-s)");
        return menuBar;
    }

    private void setTheme(boolean isDark) {
        var js = "document.documentElement.setAttribute('theme', $0)";
        getElement().executeJs(js, isDark ? Lumo.DARK : Lumo.LIGHT);
    }

    private RouteTabs tabs() {
        RouteTabs tabs = new RouteTabs();
        tabs.add(routerLink(getTranslation("app.department.title"), DepartmentView.class), routerLink(getTranslation("app.employee.title"), EmployeeView.class));
        tabs.addThemeVariants(TabsVariant.LUMO_CENTERED);
        tabs.getStyle().set("margin", "0 auto");
        return tabs;
    }

    public RouterLink routerLink(String title, Class<? extends Component> clazz) {
        return new RouterLink(title, clazz);
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (event.getNavigationTarget() == MainLayout.class) {
            event.forwardTo(DepartmentView.class);
        }
    }

    /**
     * Вкладки для корректного отображения текущей вкладки
     */
    private static class RouteTabs extends Tabs implements BeforeEnterObserver {
        private final Map<RouterLink, Tab> routerLinkTabMap = new HashMap<>();

        public void add(RouterLink... routerLink) {
            for (var route : routerLink) {
                route.setHighlightCondition(HighlightConditions.sameLocation());
                route.setHighlightAction(
                        (link, shouldHighlight) -> {
                            if (shouldHighlight) setSelectedTab(routerLinkTabMap.get(route));
                        }
                );
                routerLinkTabMap.put(route, new Tab(route));
                add(routerLinkTabMap.get(route));
            }
        }

        @Override
        public void beforeEnter(BeforeEnterEvent event) {
            // если ни одна таба не будет совпадать
            setSelectedTab(null);
        }
    }
}
