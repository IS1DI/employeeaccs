package ru.aisa.employeeaccs.routes;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.server.PWA;

@PWA(name = "EmployeeAccs", shortName = "EAS")
public class AppShell implements AppShellConfigurator {
}
