package ru.aisa.employeeaccs.routes;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;

import java.util.Locale;

public class LocaleChanger extends Div {
    public LocaleChanger() {
        var changeLocaleCombo = new ComboBox<Locale>(getTranslation("app.locale.changer"), LocalesProvider.LOCALES);
        changeLocaleCombo.setValue(getLocale());
        changeLocaleCombo.setItemLabelGenerator(l -> getTranslation("lang." + l.getLanguage().toLowerCase()));
        changeLocaleCombo.addValueChangeListener(event -> getUI().ifPresent(ui -> {
            ui.getSession().setLocale(event.getValue());
            ui.getPage().reload();
        }));
        add(changeLocaleCombo);
    }
}
