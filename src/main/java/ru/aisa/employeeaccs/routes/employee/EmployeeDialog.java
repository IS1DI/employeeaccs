package ru.aisa.employeeaccs.routes.employee;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.data.VaadinSpringDataHelpers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import ru.aisa.employeeaccs.events.CreateEntityEvent;
import ru.aisa.employeeaccs.events.UpdateEntityEvent;
import ru.aisa.employeeaccs.model.dto.DepartmentDto;
import ru.aisa.employeeaccs.model.dto.EmployeeDto;

import java.time.LocalDate;
import java.util.Optional;
import java.util.function.Function;

public class EmployeeDialog extends Dialog {
    private final TextField firstName = new TextField(getTranslation("emp.view.firstName.label"));
    private final TextField middleName = new TextField(getTranslation("emp.view.middleName.label"));
    private final TextField lastName = new TextField(getTranslation("emp.view.lastName.label"));
    private final DatePicker birthDate = new DatePicker(getTranslation("emp.view.birthDate.label"), LocalDate.now());
    private final ComboBox<DepartmentDto.Output> depComboBox = new ComboBox<>(getTranslation("dep.view.simpleName.label"));

    /**
     * Mодальное окно для создания нового сотрудника
     *
     * @param departmentProvider функция для получения страницы отедлов
     * @param departmentGetter   функция для получения отдела по id
     * @see #EmployeeDialog(Long, EmployeeDto.Update, Function departmentProvider, Function departmentGetter)
     */
    public EmployeeDialog(Function<PageRequest, Page<DepartmentDto.Output>> departmentProvider,
                          Function<Long, DepartmentDto.Output> departmentGetter) {
        var dto = new EmployeeDto.Create();
        var binder = new Binder<>(EmployeeDto.Create.class);
        binder.setBean(dto);
        add(createForm(binder, departmentProvider, departmentGetter));
        var cancelBtn = new Button(getTranslation("emp.view.modal.cancel"), e -> this.close());
        var submitBtn = new Button(getTranslation("emp.view.modal.submit"), e -> {
            if (binder.validate().isOk()) {
                fireEvent(new CreateEntityEvent<>(binder.getBean(), this, true));
                this.close();
            }
        });
        submitBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        getFooter().add(cancelBtn, submitBtn);
    }

    /**
     * Mодальное окно для обновления сотрудника
     *
     * @param id                 обновляемого сотрудника
     * @param dto                содержащее данные для редактирования
     * @param departmentProvider функция для получения страницы отедлов
     * @param departmentGetter   функция для получения отдела по id
     * @see #EmployeeDialog(Function departmentProvider, Function departmentGetter)
     */
    public EmployeeDialog(Long id,
                          EmployeeDto.Update dto,
                          Function<PageRequest, Page<DepartmentDto.Output>> departmentProvider,
                          Function<Long, DepartmentDto.Output> departmentGetter) {
        var binder = new Binder<>(EmployeeDto.Update.class);
        binder.setBean(dto);
        add(createForm(binder, departmentProvider, departmentGetter));
        var cancelBtn = new Button(getTranslation("emp.view.modal.cancel"), e -> this.close());
        var updateBtn = new Button(getTranslation("emp.view.modal.update"), e -> {
            if (binder.validate().isOk()) {
                fireEvent(new UpdateEntityEvent<>(id, binder.getBean(), this, true));
                this.close();
            }
        });
        updateBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        getFooter().add(cancelBtn, updateBtn);
    }

    /**
     * Создает биндер формы и саму форму. Форму добавляет в модальное окно
     *
     * @param binder             биндер формы
     * @param departmentProvider функция для получения страницы отделов
     * @param departmentGetter   функция для получения отдела по id из бд
     * @param <DTO>              дто для создания пользователя или обновления
     */
    private <DTO> FormLayout createForm(Binder<DTO> binder,
                              Function<PageRequest, Page<DepartmentDto.Output>> departmentProvider,
                              Function<Long, DepartmentDto.Output> departmentGetter
    ) {
        depComboBox.setItems(q -> departmentProvider.apply(VaadinSpringDataHelpers.toSpringPageRequest(q)).stream());
        depComboBox.setItemLabelGenerator(DepartmentDto.Output::getName);
        binder.forField(firstName)
                .asRequired(getTranslation("emp.view.firstName.error.empty"))
                .bind("firstName");
        binder.forField(middleName)
                .bind("middleName");
        binder.forField(lastName)
                .asRequired(getTranslation("emp.view.lastName.error.empty"))
                .bind("lastName");
        binder.forField(birthDate)
                .asRequired(getTranslation("emp.view.birthDate.error.empty"))
                .bind("birthDate");
        binder.forField(depComboBox)
                .asRequired(getTranslation("emp.view.department.error.empty"))
                .withConverter(DepartmentDto.Output::getId, id -> Optional.ofNullable(id)
                        .map(departmentGetter)
                        .orElse(null))
                .bind("departmentId");
        var form = new FormLayout(firstName, middleName, lastName, middleName, birthDate, depComboBox);
        return form;
    }

    @SuppressWarnings({"unchecked"})
    public Registration addUpdateEntityListener(ComponentEventListener<UpdateEntityEvent<Long, EmployeeDto.Update, EmployeeDialog>> listener) {
        return addListener(UpdateEntityEvent.class, new ComponentEventListener<>() {
            @Override
            public void onComponentEvent(UpdateEntityEvent event) {
                listener.onComponentEvent(event);
            }
        });
    }

    @SuppressWarnings({"unchecked"})
    public Registration addCreateEntityListener(ComponentEventListener<CreateEntityEvent<EmployeeDto.Create, EmployeeDialog>> listener) {
        return addListener(CreateEntityEvent.class, new ComponentEventListener<>() {
            @Override
            public void onComponentEvent(CreateEntityEvent event) {
                listener.onComponentEvent(event);
            }
        });
    }
}
