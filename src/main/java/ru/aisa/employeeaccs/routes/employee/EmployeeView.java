package ru.aisa.employeeaccs.routes.employee;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.data.VaadinSpringDataHelpers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import ru.aisa.employeeaccs.model.dto.EmployeeDto;
import ru.aisa.employeeaccs.model.mapper.DepartmentMapper;
import ru.aisa.employeeaccs.model.mapper.EmployeeMapper;
import ru.aisa.employeeaccs.routes.MainLayout;
import ru.aisa.employeeaccs.service.DepartmentService;
import ru.aisa.employeeaccs.service.EmployeeService;

import java.util.List;

@Route(value = "/employee", layout = MainLayout.class)
@PageTitle("EmployeeAccs | Employees")
@Slf4j
public class EmployeeView extends VerticalLayout {
    private final EmployeeService employeeService;
    private final DepartmentService departmentService;
    private final EmployeeMapper employeeMapper;
    private final DepartmentMapper departmentMapper;

    public EmployeeView(EmployeeService employeeService,
                        EmployeeMapper employeeMapper,
                        DepartmentService departmentService,
                        DepartmentMapper departmentMapper) {
        this.employeeService = employeeService;
        this.employeeMapper = employeeMapper;
        this.departmentMapper = departmentMapper;
        this.departmentService = departmentService;
        var grid = createGrid();
        setPadding(true);
        add(grid);
        var newEmployeeBtn = new Button(getTranslation("emp.view.modal.show"), e -> {
            var newEmployeeModal = new EmployeeDialog(
                    (pageReq) -> departmentService.page(pageReq, Sort.unsorted()).map(departmentMapper::toOutput),
                    (id) -> departmentMapper.toOutput(departmentService.get(id)));
            newEmployeeModal.addCreateEntityListener(createEvent -> {
                employeeService.create(employeeMapper.toEntity(createEvent.getEntity()));
                grid.getDataProvider().refreshAll();
            });
            log.debug("opened dialog for creating employee");
            newEmployeeModal.open();
        });
        newEmployeeBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        var horAl = new HorizontalLayout();
        horAl.setJustifyContentMode(JustifyContentMode.END);
        horAl.setPadding(true);
        horAl.setWidthFull();
        horAl.add(newEmployeeBtn);
        add(horAl);
    }

    public Grid<EmployeeDto.Output> createGrid() {
        Grid<EmployeeDto.Output> grid = new Grid<>(EmployeeDto.Output.class, false);
        var firstColumn = grid.addColumn(EmployeeDto.Output::getId).setHeader(getTranslation("emp.view.id")).setSortable(true).setSortProperty("id");
        grid.addColumn(EmployeeDto.Output::getFullName).setHeader(getTranslation("emp.view.fullName")).setSortable(true).setSortProperty("lastName", "firstName", "middleName");
        grid.addColumn(EmployeeDto.Output::getBirthDate).setHeader(getTranslation("emp.view.birthDate")).setSortable(true).setSortProperty("birthDate");
        grid.addColumn(EmployeeDto.Output::getDepartmentName).setHeader(getTranslation("emp.view.dep-name")).setSortable(true).setSortProperty("department.name");
        grid.addComponentColumn(
                employee -> {
                    var btn = new Button(getTranslation("emp.view.edit.label"));
                    btn.addClickListener(buttonClickEvent -> {
                        createUpdateEmployeeListener(employee, grid);
                    });
                    return btn;
                });
        grid.addItemDoubleClickListener(doubleClickEvent -> createUpdateEmployeeListener(doubleClickEvent.getItem(), grid));
        grid.setItems(new CallbackDataProvider<>(
                q -> employeeService.page(VaadinSpringDataHelpers.toSpringPageRequest(q), VaadinSpringDataHelpers.toSpringDataSort(q)).map(employeeMapper::toOutput).stream(),
                q -> Math.toIntExact(employeeService.page(VaadinSpringDataHelpers.toSpringPageRequest(q), VaadinSpringDataHelpers.toSpringDataSort(q)).getTotalElements()),
                EmployeeDto.Output::getId
        ));
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.sort(List.of(new GridSortOrder<>(firstColumn, SortDirection.ASCENDING)));
        grid.addSortListener(sortEvent -> {
            if (sortEvent.getSortOrder().isEmpty())
                grid.sort(List.of(new GridSortOrder<>(firstColumn, SortDirection.ASCENDING)));
        });
        firstColumn.setFooter(getTranslation("emp.view.footer.totalCountLabel") + " " + employeeService.count()).setFrozen(true);
        grid.getDataProvider().addDataProviderListener(changedEvent ->
                firstColumn.setFooter(getTranslation("emp.view.footer.totalCountLabel") + " " + employeeService.count()).setFrozen(true)
        );
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        return grid;
    }

    private void createUpdateEmployeeListener(EmployeeDto.Output employee, Grid<EmployeeDto.Output> grid) {
        var dialog = new EmployeeDialog(
                employee.getId(),
                employeeMapper.map(employeeService.get(employee.getId())),
                (pageReq) -> departmentService.page(pageReq, Sort.unsorted()).map(departmentMapper::toOutput),
                (id) -> departmentMapper.toOutput(departmentService.get(id))
        );
        log.debug("opened dialog for editing employee {}", employee.getId());
        dialog.addUpdateEntityListener(event -> {
            var updatedEmployee = employeeMapper.toOutput(employeeService.update(event.getEntityId(), event.getEntity(), employeeMapper::toUpdate));
            grid.getDataProvider().refreshItem(updatedEmployee);
        });
        dialog.open();
    }
}
