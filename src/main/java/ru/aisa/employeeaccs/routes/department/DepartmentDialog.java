package ru.aisa.employeeaccs.routes.department;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.shared.Registration;
import lombok.extern.slf4j.Slf4j;
import ru.aisa.employeeaccs.events.CreateEntityEvent;
import ru.aisa.employeeaccs.model.dto.DepartmentDto;

import java.time.LocalDate;
import java.util.function.Predicate;

@Slf4j
public class DepartmentDialog extends Dialog {
    private final int CHAR_LIMIT = 200;
    private final TextArea description = new TextArea(getTranslation("dep.view.desc.label"));
    private final TextField name = new TextField(getTranslation("dep.view.name.label"));
    private final DatePicker createAt = new DatePicker(getTranslation("dep.view.createdAt.label"), LocalDate.now());

    public DepartmentDialog(Predicate<String> existsNameValidator) {
        var dto = new DepartmentDto.Create();
        var binder = new Binder<>(DepartmentDto.Create.class);
        binder.setBean(dto);
        description.setMaxLength(CHAR_LIMIT);
        description.setValueChangeMode(ValueChangeMode.EAGER);
        description.addValueChangeListener(e -> {
            e.getSource()
                    .setHelperText(e.getValue().length() + "/" + CHAR_LIMIT);
        });
        binder.forField(name)
                .asRequired(getTranslation("dep.view.name.error.empty"))
                .withValidator((input, context) -> {
                    if (existsNameValidator.test(input)) {
                        return ValidationResult.error(getTranslation("dep.view.name.error.exists"));
                    } else return ValidationResult.ok();
                })
                .bind("name");
        binder.forField(description)
                .bind("description");
        binder.forField(createAt)
                .asRequired(getTranslation("dep.view.createAt.empty"))
                .bind("createdAt");

        var form = new FormLayout();
        form.add(name, description, createAt);
        add(form);
        var cancelBtn = new Button(getTranslation("dep.view.modal.cancel"), e -> {
            log.debug("department modal dialog closed");
            this.close();
        });
        var submitBtn = new Button(getTranslation("dep.view.modal.submit"), e -> {
            if (binder.validate().isOk()) {
                fireEvent(new CreateEntityEvent<>(binder.getBean(), this, true));
                this.close();
            }
        });
        submitBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        getFooter().add(cancelBtn, submitBtn);
    }

    @SuppressWarnings({"unchecked"})
    public Registration addCreateEntityListener(ComponentEventListener<CreateEntityEvent<DepartmentDto.Create, DepartmentDialog>> listener) {
        return addListener(CreateEntityEvent.class, new ComponentEventListener<>() {
            @Override
            public void onComponentEvent(CreateEntityEvent event) {
                listener.onComponentEvent(event);
            }
        });
    }
}
