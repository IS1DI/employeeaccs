package ru.aisa.employeeaccs.routes.department;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.data.VaadinSpringDataHelpers;
import lombok.extern.slf4j.Slf4j;
import ru.aisa.employeeaccs.model.dto.DepartmentDto;
import ru.aisa.employeeaccs.model.mapper.DepartmentMapper;
import ru.aisa.employeeaccs.routes.MainLayout;
import ru.aisa.employeeaccs.service.DepartmentService;

import java.util.List;

@Route(value = "/department", layout = MainLayout.class)
@PageTitle("EmployeeAccs | Departments")
@Slf4j
public class DepartmentView extends VerticalLayout {
    private final DepartmentService departmentService;
    private final DepartmentMapper departmentMapper;

    DepartmentView(DepartmentService departmentService, DepartmentMapper departmentMapper) {
        this.departmentMapper = departmentMapper;
        this.departmentService = departmentService;
        var grid = createGrid();
        add(grid);
        setPadding(true);
        var newDepartmentBtn = createModalBtn(grid);
        var horAl = new HorizontalLayout();
        horAl.setJustifyContentMode(JustifyContentMode.END);
        horAl.setPadding(true);
        horAl.setWidthFull();
        horAl.add(newDepartmentBtn);
        add(horAl);
    }

    private Button createModalBtn(Grid<DepartmentDto.Output> grid) {
        var btn = new Button(getTranslation("dep.view.modal.show"), e -> {
            var newDepartmentModal = new DepartmentDialog(departmentService::nameExists);
            log.debug("opened dialog for creating department");
            newDepartmentModal.addCreateEntityListener(createEvent -> {
                departmentService.create(departmentMapper.toEntity(createEvent.getEntity()));
                grid.getDataProvider().refreshAll();
            });
            newDepartmentModal.open();
        });
        btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        return btn;
    }

    public Grid<DepartmentDto.Output> createGrid() {
        Grid<DepartmentDto.Output> grid = new Grid<>(DepartmentDto.Output.class, false);
        var firstColumn = grid.addColumn(DepartmentDto.Output::getId).setHeader(getTranslation("dep.view.id")).setSortable(true).setSortProperty("id");
        grid.addColumn(DepartmentDto.Output::getName).setHeader(getTranslation("dep.view.name")).setSortable(true).setSortProperty("name");
        grid.addColumn(DepartmentDto.Output::getDescription).setHeader(getTranslation("dep.view.desc")).setSortable(true).setSortProperty("description");
        grid.addColumn(DepartmentDto.Output::getCreatedAt).setHeader(getTranslation("dep.view.createdAt")).setSortable(true).setSortProperty("createdAt");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.sort(List.of(new GridSortOrder<>(firstColumn, SortDirection.ASCENDING)));
        grid.addSortListener(sortEvent -> {
            if (sortEvent.getSortOrder().isEmpty())
                grid.sort(List.of(new GridSortOrder<>(firstColumn, SortDirection.ASCENDING)));
        });
        grid.setItems(q -> departmentService.page(VaadinSpringDataHelpers.toSpringPageRequest(q), VaadinSpringDataHelpers.toSpringDataSort(q)).stream().map(departmentMapper::toOutput));
        firstColumn.setFooter(getTranslation("dep.view.footer.totalCountLabel") + " " + departmentService.count()).setFrozen(true);
        grid.getDataProvider().addDataProviderListener(changedEvent ->
            firstColumn.setFooter(getTranslation("dep.view.footer.totalCountLabel") + " " + departmentService.count()).setFrozen(true)
        );
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        return grid;
    }
}
