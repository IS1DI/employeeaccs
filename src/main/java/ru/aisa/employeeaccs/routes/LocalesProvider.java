package ru.aisa.employeeaccs.routes;

import com.vaadin.flow.i18n.I18NProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

@RequiredArgsConstructor
@Component
public class LocalesProvider implements I18NProvider {
    static final Locale EN = Locale.US;
    static final Locale RU = new Locale("ru", "RU");
    static final List<Locale> LOCALES = List.of(EN, RU);
    private final MessageSource messageSource;

    @Override
    public List<Locale> getProvidedLocales() {
        return LOCALES;
    }

    @Override
    public String getTranslation(String key, Locale locale, Object... params) {
        return messageSource.getMessage(key, params, locale);
    }
}
