package ru.aisa.employeeaccs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.aisa.employeeaccs.model.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
