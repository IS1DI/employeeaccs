package ru.aisa.employeeaccs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.aisa.employeeaccs.model.entity.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
    boolean existsByName(String name);
}
