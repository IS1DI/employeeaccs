package ru.aisa.employeeaccs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeAccsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeAccsApplication.class, args);
    }

}
