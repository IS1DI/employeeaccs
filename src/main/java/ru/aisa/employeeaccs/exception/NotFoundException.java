package ru.aisa.employeeaccs.exception;

import lombok.Getter;
import ru.aisa.employeeaccs.message.MessageMethod;

@Getter
public class NotFoundException extends RuntimeException {
    private final String messageCode;
    private final Object[] args;

    public NotFoundException(MessageMethod method, Object... args) {
        super();
        messageCode = method.getMessageCode();
        this.args = args;
    }
}
