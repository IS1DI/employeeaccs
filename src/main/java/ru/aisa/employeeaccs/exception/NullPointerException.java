package ru.aisa.employeeaccs.exception;

import lombok.Getter;
import ru.aisa.employeeaccs.message.MessageMethod;

@Getter
public class NullPointerException extends RuntimeException {
    private final String messageCode;
    private final Object[] args;

    public NullPointerException(MessageMethod method, Object... args) {
        super();
        messageCode = method.getMessageCode();
        this.args = args;
    }
}
